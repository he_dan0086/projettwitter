package rmi;

import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

import persistence.DataBase;
import persistence.Topic;
import persistence.User;

public class Serveur {
	public static void main(String[] args) {
		User dan = new User("dan", "123");
		User marina = new User("marina", "123");
		User toto = new User("fangfang", "123");

		dan.getAbonners().push(marina);
		marina.getAbonnement().push(dan);

		marina.getAbonners().push(dan);
		dan.getAbonnement().push(marina);

		Topic topic = new Topic("guerre");
		DataBase.getTopics().put("guerre", topic);
		// User visiteur = new User();
		DataBase.getDataBase().put(dan.getUserName(), dan);
		DataBase.getDataBase().put(marina.getUserName(), marina);
		DataBase.getDataBase().put(toto.getUserName(), toto);

		// DataBase.getDataBase().put(visiteur.getUserName(),visiteur);
		Tweeter t1 = new Tweeter("je viens de manger", dan.getUserName());
		Tweeter t4 = new Tweeter("je vais dormir", toto.getUserName());
		Tweeter t2 = new Tweeter("je vais manger", marina.getUserName());
		Tweeter t3 = new Tweeter("je viens de me lever", dan.getUserName());
		Tweeter t5 = new Tweeter("je viens de me lever", toto.getUserName());
		Tweeter t6 = new Tweeter("je viens de me lever", toto.getUserName());

		DataBase.getTweets().push(t1);
		DataBase.getTweets().push(t4);
		DataBase.getTweets().push(t2);
		DataBase.getTweets().push(t3);
		DataBase.getTweets().push(t5);
		DataBase.getTweets().push(t6);

		dan.getInteresse().push(t1);
		marina.getInteresse().push(t1);

		marina.getInteresse().push(t2);
		dan.getInteresse().push(t2);

		marina.getInteresse().push(t3);
		dan.getInteresse().push(t3);

		toto.getInteresse().push(t4);
		toto.getInteresse().push(t5);
		toto.getInteresse().push(t6);

		ServeurPublic serveur_public = null;

		try {
			serveur_public = new ServeurPublicImpl();
		} catch (RemoteException e) {
			e.printStackTrace();
		}
		try {
			Registry registry = LocateRegistry.createRegistry(2004);
			System.out.println("on a lances au registry ecoutant sur 2004");
			registry.rebind("Serveur", serveur_public);

		} catch (RemoteException e) {
			e.printStackTrace();
		}
		System.out.println("Attente d invocation, Ctrl-C pour stopper!");
	}

}

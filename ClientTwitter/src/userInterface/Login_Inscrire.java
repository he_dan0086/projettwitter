package userInterface;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.HeadlessException;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.rmi.RemoteException;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.UIManager;

import rmi.Client;
import rmi.Service;

public class Login_Inscrire extends JFrame {

	private static final long serialVersionUID = 1L;
	private Client c;
	private Portail port;
	private JLabel userNameLbl;
	private JLabel passWordLbl;
	private JTextField txt_user;
	private JPasswordField txt_mdp;
	private Mypane panel;

	public Login_Inscrire(final Portail p, String title) {
		try {
			UIManager
					.setLookAndFeel("com.sun.java.swing.plaf.nimbus.NimbusLookAndFeel");
		} catch (Exception e) {
		}
		this.port = p;
		panel = new Mypane("images/background.jpg");
		this.c = p.getC();
		setTitle(title);
		setSize(300, 300);
		userNameLbl = new JLabel(new ImageIcon("images/User.png"));
		userNameLbl.setToolTipText("userName");
		this.add(userNameLbl);
		passWordLbl = new JLabel(new ImageIcon("images/Password.png"));
		passWordLbl.setToolTipText("password");
		this.add(passWordLbl);
		txt_user = new JTextField(25);
		this.add(txt_user);
		txt_mdp = new JPasswordField(25);
		this.add(txt_mdp);

		JButton titleBtn = makeButton(title);
		titleBtn.setBounds(130, 210, 90, 25);
		titleBtn.setBackground(Color.BLUE);

		this.add(titleBtn);

		JButton cancelBtn = makeButton("cancel");
		cancelBtn.setBounds(235, 210, 90, 25);
		// cancelBtn.setIcon(new ImageIcon("images/btn.png"));
		cancelBtn.setBackground(Color.blue);
		this.add(cancelBtn);

		userNameLbl.setBounds(40, 113, 160, 40);
		passWordLbl.setBounds(40, 160, 160, 40);
		txt_user.setBounds(160, 123, 160, 30);
		txt_mdp.setBounds(160, 163, 160, 30);

		this.add(panel);
		setVisible(true);
		this.setIconImage(new ImageIcon("images/twitter.png").getImage());
		this.setSize(451, 300);
		// display in the center
		int w = Toolkit.getDefaultToolkit().getScreenSize().width;
		int h = Toolkit.getDefaultToolkit().getScreenSize().height;
		this.setLocation((w - 451) / 2, (h - 300) / 2);
		this.setResizable(false);
		addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				super.windowClosing(e);
				p.setEnabled(true);
			}

		});
	}

	private JButton makeButton(final String name) {
		JButton bt = new JButton(name);
		bt.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		bt.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				if (name.equals("cancel")) {
					txt_user.setText("");
					txt_mdp.setText("");
				}
				if (name.equals("inscrire")) {
					if (txt_user.getText().equals("")
							|| txt_mdp.getText().equals(""))
						JOptionPane.showMessageDialog(null,
								"UserName or PassWord can't be empty!");
					else {
						c.setUserName(txt_user.getText());
						c.setPassword(txt_mdp.getText());
						try {
							if (c.getServeur().inscrire(c)) {
								JOptionPane.showMessageDialog(null,
										"Succes to inscript");
								dispose();
								port.setEnabled(true);
							} else {
								JOptionPane.showMessageDialog(null,
										"The peusdo already exists!!");
							}
						} catch (HeadlessException e) {
							e.printStackTrace();
						} catch (RemoteException e) {
							e.printStackTrace();
						}
					}
				}
				if (name.equals("connecter")) {
					if (txt_user.getText().equals("")
							|| txt_mdp.getText().equals(""))
						JOptionPane.showMessageDialog(null,
								"UserName or PassWord can't be empty!");
					else {
						c.setUserName(txt_user.getText());
						c.setPassword(txt_mdp.getText());
						try {
							Service s = c.getServeur().logon(c);
							if (s != null){
								c.setService(s);
								JOptionPane.showMessageDialog(null,
										"Succes to login");
							}
							else {
								JOptionPane.showMessageDialog(null,
										"Username or password is wrong!");
								return;
							}
							c.setConnecter(true);
							dispose();
							MainWindow mw = new MainWindow(port.getC());
							port.dispose();

						} catch (RemoteException e) {
							e.printStackTrace();
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				}
			}
		});
		return bt;
	}

}

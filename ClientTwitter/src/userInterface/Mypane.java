package userInterface;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.File;

import javax.imageio.ImageIO;
import javax.swing.JPanel;

public class Mypane extends JPanel {
	String path;
	public Mypane(String background){
		path=background;
		
	}
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		try {
			BufferedImage img = ImageIO.read(new File(path));
			//autoresize
			g.drawImage(img, 0, 0, this.getWidth(), this.getHeight(), null);
		} catch (Exception e) {
		}
		Font f = new Font("Noir", Font.BOLD, 20);
		g.setFont(f);
		g.setColor(new Color(0, 0, 0));
	}
}
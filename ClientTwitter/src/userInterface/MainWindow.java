package userInterface;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.rmi.RemoteException;
import java.util.Stack;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.UIManager;

import rmi.Client;
import rmi.Tweeter;

public class MainWindow extends JFrame {
	private Client c;
	private Mypane panel;
	private JPanel bt_panel;
	private JTextField contenu;
	private Topic top;
	private JScrollPane scoll;
	private static final long serialVersionUID = 1L;

	public Client getC() {
		return c;
	}

	public void setC(Client c) {
		this.c = c;
	}

	public MainWindow(Client client) throws RemoteException {
		// lookandfeel
		try {
			UIManager
					.setLookAndFeel("com.sun.java.swing.plaf.nimbus.NimbusLookAndFeel");
		} catch (Exception e) {
		}
		this.c = client;
		this.c.setMw(this);
		panel = new Mypane("images/bg.jpg");
		panel.setLayout(new GridLayout(0, 1));
		scoll = new JScrollPane(panel);

		setTitle("Welcome " + this.c.getUserName());
		setSize(500, 500);
		// display in the center
		int w = Toolkit.getDefaultToolkit().getScreenSize().width;
		int h = Toolkit.getDefaultToolkit().getScreenSize().height;
		this.setLocation((w - 451) / 2, (h - 300) / 2);

		JPanel aj_panel = new JPanel(new GridLayout(0, 2));
		contenu = new JTextField();
		aj_panel.add(contenu);
		makeButton("publier", aj_panel);

		Stack<Tweeter> list = c.getService().listerAbonnement();
		refresh(list);
		this.getContentPane().setLayout(new BorderLayout());
		this.getContentPane().add(aj_panel, BorderLayout.NORTH);
		this.getContentPane().add(scoll, BorderLayout.CENTER);

		bt_panel = new JPanel();
		bt_panel.setLayout(new GridLayout(1, 3));
		makeButton("deconnecter");
		makeButton("abonner");
		// makeButton("desinscrire");
		makeButton("sujet");
		this.getContentPane().add(bt_panel, BorderLayout.SOUTH);
		setVisible(true);

		addWindowListener(new WindowAdapter() {

			public void windowClosing(WindowEvent e) {
				super.windowClosing(e);
				if (top != null)
					top.dispose();
				try {
					c.getServeur().remotedeconnecter(c);
				} catch (RemoteException e1) {
					e1.printStackTrace();
				}
				// c.setServeur(null);
				// c.setService(null);
				// c.setMw(null);
				// c.setTopic(null);
				// c.setConnecter(false);
				// c = null;
				System.exit(0);
			}
		});
	}

	private void makeButton(final String name) {
		JButton bt = new JButton(name);
		bt_panel.add(bt);
		bt.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				if (name.equals("deconnecter")) {

					c.setConnecter(false);
					try {
						Portail nouPort = new Portail(c);
						if (top != null)
							top.dispose();
						c.setPortail(nouPort);
						c.setService(null);
						c.setMw(null);
						c.setTopic(null);

					} catch (RemoteException e) {
						e.printStackTrace();
					}
					dispose();
				}
				if (name.equals("abonner")) {
					setEnabled(false);
					new Abonner(MainWindow.this);
				}
				if (name.equals("sujet")) {
					try {
						top = new Topic(c);
					} catch (RemoteException e) {
						e.printStackTrace();
					}
					c.setTopic(top);
				}
			}

		});
	}

	private void makeButton(final String name, JPanel panel) {
		JButton bt = new JButton(name);
		panel.add(bt);
		bt.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				if (name.equals("publier")) {
					try {
						String text = contenu.getText();
						String[] matchers = text.split("#");

						if (text.equals("")) {
							JOptionPane.showMessageDialog(null,
									"votre tweet ne peut pas être vide");
							return;
						}
						if (matchers.length != 3)
							c.getService().publier(text);
						else
							c.getService().publier(text, matchers[1]);
						contenu.setText("");
					} catch (RemoteException e) {
						e.printStackTrace();
					}
				}
			}

		});
	}

	private void makeButton(final String name, JPanel panel, final int id) {
		JButton bt = new JButton(name);
		bt.setBackground(Color.blue);
		panel.add(bt);
		bt.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				if (name.equals("supprimer")) {
					try {
						c.getService().supprimer(id);
					} catch (RemoteException e) {
						e.printStackTrace();
					}
				}
			}

		});
	}

	private void makeButton(final String name, JPanel panel,
			final String deteste) {
		JButton bt = new JButton(name);
		panel.add(bt);
		bt.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				if (name.equals("desabonner")) {
					try {
						c.getService().desabonner(deteste);
					} catch (RemoteException e) {
						e.printStackTrace();
					}
				}
			}

		});
	}

	/**
	 * 
	 * @param listerAbonnement
	 * @throws RemoteException
	 */
	public void refresh(Stack<Tweeter> listerAbonnement) throws RemoteException {

		panel.removeAll();
		for (int i = listerAbonnement.size() - 1; i >= 0; i--) {
			JLabel label1 = new JLabel(listerAbonnement.get(i).getEditer());
			JLabel label2 = new JLabel(listerAbonnement.get(i).getContenu());
			JLabel label3 = new JLabel(listerAbonnement.get(i).getId() + "");
			Mypane mypane = new Mypane("images/bg.jpg");
			mypane.setLayout(new GridLayout(1, 3));
			mypane.add(label1);
			mypane.add(label2);
			mypane.add(label3);
			panel.add(mypane);
			if (listerAbonnement.get(i).getEditer().equals(c.getUserName())) {

				makeButton("supprimer", mypane, listerAbonnement.get(i).getId());
			} else {

				makeButton("desabonner", mypane, listerAbonnement.get(i)
						.getEditer());

			}
		}
		panel.repaint();
		this.validate();
	}

}

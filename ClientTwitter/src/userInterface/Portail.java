package userInterface;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.rmi.RemoteException;
import java.util.Stack;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.UIManager;

import rmi.Client;
import rmi.Tweeter;

public class Portail extends JFrame {
	private static final long serialVersionUID = 1L;
	private Mypane panel;
	private JPanel bt_panel;
	private Client c;
	private JScrollPane scroll;

	public Client getC() {
		return c;
	}

	public void setC(Client c) {
		this.c = c;
	}

	public Portail(Client client) throws RemoteException {
		// lookandfeel
		try {
			UIManager
					.setLookAndFeel("com.sun.java.swing.plaf.nimbus.NimbusLookAndFeel");
		} catch (Exception e) {
			e.printStackTrace();
		}
		//init
		this.c = client;
		panel = new Mypane("images/bg.jpg");
		panel.setLayout(new GridLayout(0, 3));
		scroll=new JScrollPane(panel);
		bt_panel = new JPanel();
		bt_panel.setLayout(new GridLayout(1, 2));
		makeButton("connecter");
		makeButton("inscrire");
		
		Stack<Tweeter> list = c.getServeur().listerTout(c);
		refresh(list);
		
		this.add(scroll, BorderLayout.CENTER);
		this.add(bt_panel, BorderLayout.SOUTH);
		
		setTitle("Welcome");
		setSize(400, 400);
		setVisible(true);
		// display in the center
		int w = Toolkit.getDefaultToolkit().getScreenSize().width;
		int h = Toolkit.getDefaultToolkit().getScreenSize().height;
		this.setLocation((w - 451) / 2, (h - 300) / 2);
		addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				super.windowClosing(e);
				try {
					c.getServeur().remotedeconnecter(c);
				} catch (RemoteException e1) {
					e1.printStackTrace();
				}
				System.exit(0);
			}
		});

		this.setIconImage(new ImageIcon("images/twitter.png").getImage());
	}
	/**
	 * fresh the content of the list
	 * @param listerTout
	 */
	public void refresh(Stack<Tweeter> listerTout) {
		panel.removeAll();
		//head of the list
		Font f=new Font("Noir",Font.BOLD,15);
		JLabel l1 = new JLabel("User");
		l1.setFont(f);
		JLabel l2 = new JLabel("Status");
		l2.setFont(f);
		JLabel l3 = new JLabel("ID");
		l3.setFont(f);
		panel.add(l1);
		panel.add(l2);
		panel.add(l3);
		
		//display all of status according to public time order
		for (int i = listerTout.size() - 1; i >= 0; i--) {
			JLabel label1 = new JLabel(listerTout.get(i).getEditer());
			JLabel label2 = new JLabel(listerTout.get(i).getContenu());
			JLabel label3 = new JLabel(listerTout.get(i).getId() + "");
			panel.add(label1);
			panel.add(label2);
			panel.add(label3);
		}
		panel.repaint();
		this.validate();
	}

	/**
	 * create a button and add it to the bt_panel
	 * @param name
	 */
	private void makeButton(final String name) {
		JButton bt = new JButton(name);
		bt.setBackground(Color.BLUE);
		bt_panel.add(bt);
		bt.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				setEnabled(false);
				new Login_Inscrire(Portail.this, name);
			}
		});
	}

}
